var express = require("express");
var router = express.Router();
const bcrypt = require("bcryptjs");
const knex = require("../models/connection");


/**
 * validateEmail to check the given data is true or false
 * @param {*} email
 */
let validateEmail = async (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let responseValue = await re.test(String(email).toLowerCase());
  return responseValue;
};
/**
 * getUser function to get the user detail by checking Email ID
 * @param {String} payload
 */
let getUser = async (payload) => {
  try {
    let checkEmailOrPhone = await validateEmail(payload);
    if (checkEmailOrPhone === true) {
      let insertData = {
        w_email: payload,
      };
      let data = await knex("worker").where(insertData).first();
      return data;
    } else {
      throw "email not valid!!";
    }
  } catch (error) {
    throw error;
  }
};

/**
 * comparePass function to check the login password correct
 * @param {String} userPassword
 * @param {String} databasePassword
 */
let comparePass = async (userPassword, databasePassword) => {
  let data = await bcrypt.compareSync(userPassword, databasePassword);
  return data;
};

/**
 * @param {String} email
 * @param {String} password
 */
let login = async (email, password) => {
  try {
    let user = await getUser(email);
    if (user == undefined) {
      throw "Invalid Username or Password";
    } else if (user.w_status == 0) {
      throw "Your account is disabled";
    }
    let passwordMatched = await comparePass(password, user.w_password);
    if (passwordMatched) {
      delete user.w_password;
      return user;
    } else {
      throw "Invalid Username or Password";
    }
  } catch (error) {
    throw error;
  }
};

/**
 * @param {String} email
 * @param {String} password
 */
let profile = async (id) => {
  try {
    let user = await knex("worker").where({ w_id: id }).first();
    if (user == undefined) {
      throw "Invalid Username or Password";
    } else if (user.w_status == 0) {
      throw "Your account is disabled";
    }
    delete user.w_password;
    return user;
  } catch (error) {
    throw error;
  }
};

/**
 * @param {String} email
 * @param {String} password
 */
let changepassword = async (id, oldpassword, newpassword) => {
  try {
    let user = await knex("worker").where({ w_id: id }).first();
    if (user == undefined) {
      throw "Invalid Username or Password";
    } else if (user.w_status == 0) {
      throw "Your account is disabled";
    }
    let passwordMatched = await comparePass(oldpassword, user.w_password);
    if (!passwordMatched) {
        throw "Invalid Old Password";
    }
    let salt = bcrypt.genSaltSync();
    var hash = bcrypt.hashSync(newpassword, salt);
    await knex("worker").where({ w_id: id }).update({w_password: hash})
    
    return true;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  login,
  profile,
  changepassword,
};
