const bcrypt = require("bcryptjs");
const knex = require("../models/connection");

/**
 * createUser function for added new user
 * @param {Object} payload
 */
let create = async (payload) => {
  try {
    let insertData = {
      t_name: payload.title,
      t_desc: payload.desc,
      t_days: payload.days,
      t_points: payload.points,
      t_points: payload.points,
      t_m_id: payload.managerid,
    };
    let response = await knex("task").insert(insertData).returning("t_id");
    insertData = response[0];
    return insertData;
  } catch (error) {
    throw error;
  }
};

let getTasks = async (mid, taskstatus = false,queryParam) => {
  try {
    let page_start = 0;
    if (!queryParam.page_no) {
        page_start = 0;
    } else {
        page_start = queryParam.page_no * 10;
    }
    let insertData = {
      t_m_id: mid,
      t_status: 1,
      t_is_deleted: 0,
    };
    if (taskstatus) {
      insertData.t_task_status = taskstatus;
    }
    let response = await knex("task").where(insertData).returning("t_id").limit(10).offset(page_start);
    insertData = response;
    return insertData;
  } catch (error) {
    throw error;
  }
};

let statusupdate = async (payload) => {
  try {
    let insertData = {
      t_task_status: payload.status,
    };
    let response = await knex("task")
      .update(insertData)
      .where({ t_id: payload.taskid });
    insertData = response[0];
    return insertData;
  } catch (error) {
    throw error;
  }
};

let workerUpdate = async (tid, wid) => {
  try {
    let insertData = {
      t_w_id: wid,
      t_task_status: "Pending",
    };
    let response = await knex("task").update(insertData).where({ t_id: tid });
    insertData = response[0];
    return insertData;
  } catch (error) {
    throw error;
  }
};

let getWorkerTasks = async (wid, taskstatus = false, queryParam) => {
  try {
    let page_start = 0;
    if (!queryParam.page_no) {
        page_start = 0;
    } else {
        page_start = queryParam.page_no * 10;
    }
    let insertData = {
      t_w_id: wid,
      t_status: 1,
      t_is_deleted: 0,
    };
    if (taskstatus) {
      insertData.t_task_status = taskstatus;
    }
    let response = await knex("task").where(insertData).returning("t_id").limit(10).offset(page_start);

    insertData = response;
    return insertData;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  create,
  getTasks,
  statusupdate,
  workerUpdate,
  getWorkerTasks,
};
