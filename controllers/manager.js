const bcrypt = require("bcryptjs");
const knex = require("../models/connection");

let checkEmailExist = async (email) => {
  try {
    let response = await knex("manager")
      .column("m_email")
      .where({
        m_email: email,
      })
      .first();
    if (response !== undefined) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    throw error;
  }
};

/**
 * createUser function for added new user
 * @param {Object} payload
 */
let create = async (payload) => {
  try {
    let checkemail = await checkEmailExist(payload.email);
    if (checkemail == true) {
      throw "email already exist!!";
    }
    if (payload.password) {
      let salt = bcrypt.genSaltSync();
      var hash = bcrypt.hashSync(payload.password, salt);
    } else {
      var hash = "";
    }
    let insertData = {
      m_fname: payload.firstname,
      m_lname: payload.lastname,
      m_email: payload.email,
      m_password: hash,
    };
    let response = await knex("manager").insert(insertData).returning("m_id");
    insertData = response[0];
    return insertData;
  } catch (error) {
    throw error;
  }
};
/**
 * validateEmail to check the given data is true or false
 * @param {*} email
 */
let validateEmail = async (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let responseValue = await re.test(String(email).toLowerCase());
  return responseValue;
};
/**
 * getUser function to get the user detail by checking Email ID
 * @param {String} payload
 */
let getUser = async (payload) => {
  try {
    let checkEmailOrPhone = await validateEmail(payload);
    if (checkEmailOrPhone === true) {
      let insertData = {
        m_email: payload,
      };
      let data = await knex("manager").where(insertData).first();
      return data;
    } else {
      throw "email not valid!!";
    }
  } catch (error) {
    throw error;
  }
};

/**
 * comparePass function to check the login password correct
 * @param {String} userPassword
 * @param {String} databasePassword
 */
let comparePass = async (userPassword, databasePassword) => {
  let data = await bcrypt.compareSync(userPassword, databasePassword);
  return data;
};

/**
 * @param {String} email
 * @param {String} password
 */
let login = async (email, password) => {
  try {
    let user = await getUser(email);
    if (user == undefined) {
      throw "Invalid Username or Password";
    } else if (user.m_status == 0) {
      throw "Your account is disabled";
    }
    let passwordMatched = await comparePass(password, user.m_password);
    if (passwordMatched) {
      delete user.m_password;
      return user;
    } else {
      throw "Invalid Username or Password";
    }
  } catch (error) {
    throw error;
  }
};

let checkEmpExist = async (email) => {
  try {
    let response = await knex("worker")
      .column("w_email")
      .where({
        w_email: email,
      })
      .first();
    if (response !== undefined) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    throw error;
  }
};

/**
 * createUser function for added new user
 * @param {Object} payload
 */
let createWorker = async (payload) => {
  try {
    let checkemail = await checkEmpExist(payload.email);
    if (checkemail == true) {
      throw "email already exist!!";
    }
    if (payload.password) {
      let salt = bcrypt.genSaltSync();
      var hash = bcrypt.hashSync(payload.password, salt);
    } else {
      var hash = "";
    }
    let insertData = {
      w_fname: payload.firstname,
      w_lname: payload.lastname,
      w_email: payload.email,
      w_password: hash,
      w_m_id: payload.managerid,
    };
    let response = await knex("worker").insert(insertData).returning("w_id");
    insertData = response[0];
    return insertData;
  } catch (error) {
    throw error;
  }
};

/**
 * createUser function for added new user
 * @param {String} mid
 */
let getWorkers = async (mid) => {
  try {
    let quirey = {
      w_m_id: mid,
      w_status: 1,
      w_is_deleted: 0,
    };
    let response = await knex("worker").where(quirey);
    insertData = response;
    return insertData;
  } catch (error) {
    throw error;
  }
};

/**
 * @param {String} email
 * @param {String} password
 */
let profile = async (id) => {
  try {
    let user = await knex("manager").where({ m_id: id }).first();
    if (user == undefined) {
      throw "Invalid Username or Password";
    } else if (user.w_status == 0) {
      throw "Your account is disabled";
    }
    delete user.w_password;
    return user;
  } catch (error) {
    throw error;
  }
};

/**
 * @param {String} email
 * @param {String} password
 */
let changepassword = async (id, oldpassword, newpassword) => {
  try {
    let user = await knex("manager").where({ m_id: id }).first();
    if (user == undefined) {
      throw "Invalid Username or Password";
    } else if (user.w_status == 0) {
      throw "Your account is disabled";
    }
    let passwordMatched = await comparePass(oldpassword, user.m_password);
    if (!passwordMatched) {
      throw "Invalid Old Password";
    }
    let salt = bcrypt.genSaltSync();
    var hash = bcrypt.hashSync(newpassword, salt);
    await knex("manager").where({ m_id: id }).update({ m_password: hash });

    return true;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  create,
  login,
  createWorker,
  getWorkers,
  changepassword,
  profile,
};
