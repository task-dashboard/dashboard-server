require("dotenv").config();
const databaseName = process.env.PG_DB_DATABASE;
const testDatabaseName = process.env.PG_DB_TEST_DATABASE;
const host = process.env.PG_DB_HOST;
const port = process.env.PG_DB_PORT;
const user = process.env.PG_DB_USER;
const password = process.env.PG_DB_PASSWORD;
module.exports = {
  development: {
    client: "pg",
    version: "7.2",
    connection: {
      host: host,
      user: user,
      password: password,
      database: databaseName
    },
    migrations: {
      directory: __dirname + "/models/migrations"
    },
    seeds: {
      directory: __dirname + "/models/seeds"
    }
  },
  test: {
    client: "pg",
    version: "7.2",
    connection: `mysql://${user}:${password}@${host}/${testDatabaseName}`,
    migrations: {
      directory: __dirname + "/models/test/migrations"
    },
    seeds: {
      directory: __dirname + "/models/test/seeds"
    }
  }
};
