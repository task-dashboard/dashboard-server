
exports.up = function(knex, Promise) {
    //check the table exiest already
    return knex.schema.hasTable("task").then(function(exists) {
        if (!exists) {
        // create if not exiest
        return knex.schema.createTable("task", function(table) {
            //auto increment id
            table.increments("t_id").comment("auto increment id");
            table.string("t_name", [100]);
            table.text("t_desc", "longtext");
            table.integer("t_days").defaultTo('0');
            table.integer("t_points").defaultTo('0');
            table.enu('t_task_status', ['Unassigned', 'Pending', 'Completed']).defaultTo("Unassigned")
            
             //fantastic five columns should be loadded to all the tables
            table.timestamp("t_added_dt").defaultTo(knex.fn.now()).comment(
                "this should be having the date time when this entry added into table"
            );
            table.timestamp("t_updated_dt").nullable().comment(
                "this should be having the date time when this entry updated"
            );
            table.boolean("t_is_deleted").defaultTo(0).comment(
                "when this column is 0 = is not deleted, and when it is 1 this entry consided as deleted"
            );
            table.timestamp("t_is_deleted_dt").nullable().comment(
                "this column should be having the date time of when this entry is deleted"
            );
            table.boolean("t_status").defaultTo(1).comment(
                "its a genaral status of this entry, for development purpose again if it is zero this entry should be hidded to users"
            );
        });
        }
    });
};

exports.down = function(knex, Promise) {
    //check the table exiest already
    return knex.schema.hasTable("task").then(function(exists) {
        if (exists) {
            // drop if exiest
            return knex.schema.dropTable("task");
        }
    })
};
