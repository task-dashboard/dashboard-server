
exports.up = function(knex, Promise) {
    //check the table exiest already
    return knex.schema.hasTable("task").then(function(exists) {
        if (exists) {
            // create if not exiest
            return knex.schema.alterTable("task", function(table) {
                table.bigInteger("t_w_id").nullable().alter()
            });
        }
    });
};

exports.down = function(knex, Promise) {
    //check the table exiest already
    return knex.schema.hasTable("task").then(function(exists) {
        if (exists) {
            // create if not exiest
            return knex.schema.alterTable("task", function(table) {
                table.bigInteger("t_w_id").notNullable().alter()
            });
        }
    });
  
};
