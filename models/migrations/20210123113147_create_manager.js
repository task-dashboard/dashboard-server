
exports.up = function(knex, Promise) {
    //check the table exiest already
    return knex.schema.hasTable("manager").then(function(exists) {
        if (!exists) {
        // create if not exiest
        return knex.schema.createTable("manager", function(table) {
             //auto increment id
             table.increments("m_id").comment("auto increment id");
             table.string("m_fname", [100]);
             table.string("m_lname", [100]);
             table.string("m_email", [100]);
             table.string("m_password", [255]);

            //fantastic five columns should be loadded to all the tables
            table.timestamp("m_added_dt").defaultTo(knex.fn.now()).comment(
                "this should be having the date time when this entry added into table"
            );
            table.timestamp("m_updated_dt").nullable().comment(
                "this should be having the date time when this entry updated"
            );
            table.boolean("m_is_deleted").defaultTo(0).comment(
                "when this column is 0 = is not deleted, and when it is 1 this entry consided as deleted"
            );
            table.timestamp("m_is_deleted_dt").nullable().comment(
                "this column should be having the date time of when this entry is deleted"
            );
            table.boolean("m_status").defaultTo(1).comment(
                "its a genaral status of this entry, for development purpose again if it is zero this entry should be hidded to users"
            );

        });
        }
    });
};

exports.down = function(knex, Promise) {
    //check the table exiest already
    return knex.schema.hasTable("requirement").then(function(exists) {
        if (exists) {
            // drop if exiest
            return knex.schema.dropTable("requirement");
        }
    })
};
