
exports.up = function(knex, Promise) {
    //check the table exiest already
    return knex.schema.hasTable("worker").then(function(exists) {
        if (exists) {
        // create if not exiest
        return knex.schema.alterTable("worker", function(table) {
            table.bigInteger("w_m_id").unsigned().notNullable().comment("managers table auto increment id");
            //forign key configrations
            table
            .foreign("w_m_id")
            .references("manager.m_id")
        });
        }
    });
};

exports.down = function(knex, Promise) {
    //check the table exiest already
    return knex.schema.hasTable("worker").then(function(exists) {
        if (exists) {
            return knex.schema.alterTable('worker', function(table) {
                table.dropColumn('w_m_id');
                table.dropForeign('w_m_id')
            });
        }
    })
};
