
exports.up = function(knex, Promise) {
    //check the table exiest already
    return knex.schema.hasTable("task").then(function(exists) {
        if (exists) {
        // create if not exiest
        return knex.schema.alterTable("task", function(table) {
            table.bigInteger("t_m_id").unsigned().notNullable().comment("managers table auto increment id");
            table.bigInteger("t_w_id").unsigned().notNullable().comment("worker table auto increment id");
            //forign key configrations
            table
            .foreign("t_m_id")
            .references("manager.m_id")
            table
            .foreign("t_w_id")
            .references("worker.w_id")
        });
        }
    });
};

exports.down = function(knex, Promise) {
      //check the table exiest already
      return knex.schema.hasTable("task").then(function(exists) {
        if (exists) {
            return knex.schema.alterTable('task', function(table) {
                table.dropColumn('t_m_id');
                table.dropColumn('t_w_id');
                table.dropForeign('t_m_id')
                table.dropForeign('t_w_id')
            });
        }
    })
};
