var express = require("express");
var router = express.Router();
const { check, oneOf, validationResult } = require("express-validator/check");
const manager_ctrl = require("../controllers/manager");
const task_ctrl = require("../controllers/task");

router.post(
  "/signUp",
  [
    check("firstname", "firstname is required").isLength({ min: 1 }).exists(),
    check("email", "email is required").isEmail().isLength({ min: 1 }).exists(),
    check("password", "password is required").isLength({ min: 4 }).exists(),
  ],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      let creatUserResult = await manager_ctrl.create(req.body);
      responseFormat(res, 200, "success", creatUserResult);
    } catch (error) {
      console.log(error);
      if (error.constraint == null && error.message == undefined) {
        responseFormat(res, 409, "failed", {
          error: error,
        });
      } else if (error.message != undefined) {
        responseFormat(res, 409, "failed", {
          error: error.mapped(),
        });
      } else {
        let errName = error.constraint.split("_");
        responseFormat(res, 422, "failed", {
          error: `${errName[2]} already exists`,
        });
      }
    }
  }
);

router.post(
  "/login",
  [
    check("email", "email is required").isEmail().isLength({ min: 1 }).exists(),
    check("password", "password is required").isLength({ min: 1 }).exists(),
  ],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      const email = req.body.email;
      const password = req.body.password;
      let responseData = await manager_ctrl.login(email, password);
      if (responseData) {
        responseFormat(res, 200, "success", responseData);
      } else {
        console.log(error);
        responseFormat(res, 409, "failed", responseData);
      }
    } catch (error) {
      console.log(error);
      responseFormat(res, 409, "failed", error);
    }
  }
);

router.post(
  "/:mid/worker",
  [
    check("firstname", "firstname is required").isLength({ min: 1 }).exists(),
    check("email", "email is required").isEmail().isLength({ min: 1 }).exists(),
    check("password", "password is required").isLength({ min: 4 }).exists(),
  ],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      req.body.managerid = req.params.mid;
      let creatUserResult = await manager_ctrl.createWorker(req.body);
      responseFormat(res, 200, "success", creatUserResult);
    } catch (error) {
      console.log(error);
      if (error.constraint == null && error.message == undefined) {
        responseFormat(res, 409, "failed", {
          error: error,
        });
      } else if (error.message != undefined) {
        responseFormat(res, 409, "failed", {
          error: error.mapped(),
        });
      } else {
        let errName = error.constraint.split("_");
        responseFormat(res, 422, "failed", {
          error: `${errName[2]} already exists`,
        });
      }
    }
  }
);

router.get("/:mid/worker", async (req, res, next) => {
  try {
    let Result = await manager_ctrl.getWorkers(req.params.mid);
    responseFormat(res, 200, "success", Result);
  } catch (error) {
    console.log(error);
    responseFormat(res, 409, "failed", {
      error: error,
    });
  }
});

router.post(
  "/:mid/task",
  [
    check("title", "Title is required").isLength({ min: 1 }).exists(),
    check("desc", "Description is required").isLength({ min: 1 }).exists(),
    check("days", "Days is required").isLength({ min: 1 }).exists(),
    check("points", "Points is required").isLength({ min: 1 }).exists(),
  ],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      req.body.managerid = req.params.mid;
      let creatUserResult = await task_ctrl.create(req.body);
      responseFormat(res, 200, "success", creatUserResult);
    } catch (error) {
      console.log(error);
      if (error.constraint == null && error.message == undefined) {
        responseFormat(res, 409, "failed", {
          error: error,
        });
      } else if (error.message != undefined) {
        responseFormat(res, 409, "failed", {
          error: error.mapped(),
        });
      } else {
        let errName = error.constraint.split("_");
        responseFormat(res, 422, "failed", {
          error: `${errName[2]} already exists`,
        });
      }
    }
  }
);

router.get("/:mid/task", async (req, res, next) => {
  try {
    taskstatus = false;
    if (req.query.status) {
      taskstatus = req.query.status;
    }
    let creatUserResult = await task_ctrl.getTasks(req.params.mid, taskstatus, req.query);
    responseFormat(res, 200, "success", creatUserResult);
  } catch (error) {
    console.log(error);
    if (error.constraint == null && error.message == undefined) {
      responseFormat(res, 409, "failed", {
        error: error,
      });
    } else if (error.message != undefined) {
      responseFormat(res, 409, "failed", {
        error: error.mapped(),
      });
    } else {
      let errName = error.constraint.split("_");
      responseFormat(res, 422, "failed", {
        error: `${errName[2]} already exists`,
      });
    }
  }
});

router.put(
  "/:mid/task/:tid",
  [check("status", "Status is required").isLength({ min: 1 }).exists()],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      req.body.managerid = req.params.mid;
      req.body.taskid = req.params.tid;
      let creatUserResult = await task_ctrl.statusupdate(req.body);
      responseFormat(res, 200, "success", creatUserResult);
    } catch (error) {
      console.log(error);
      if (error.constraint == null && error.message == undefined) {
        responseFormat(res, 409, "failed", {
          error: error,
        });
      } else if (error.message != undefined) {
        responseFormat(res, 409, "failed", {
          error: error.mapped(),
        });
      } else {
        let errName = error.constraint.split("_");
        responseFormat(res, 422, "failed", {
          error: `${errName[2]} already exists`,
        });
      }
    }
  }
);

router.put(
  "/:mid/taskassign/:tid",
  [check("worker", "worker is required").isLength({ min: 1 }).exists()],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      let creatUserResult = await task_ctrl.workerUpdate(
        req.params.tid,
        req.body.worker
      );
      responseFormat(res, 200, "success", creatUserResult);
    } catch (error) {
      console.log(error);
      if (error.constraint == null && error.message == undefined) {
        responseFormat(res, 409, "failed", {
          error: error,
        });
      } else if (error.message != undefined) {
        responseFormat(res, 409, "failed", {
          error: error.mapped(),
        });
      } else {
        let errName = error.constraint.split("_");
        responseFormat(res, 422, "failed", {
          error: `${errName[2]} already exists`,
        });
      }
    }
  }
);

router.get("/:mid/profile", async (req, res, next) => {
  try {
    let creatUserResult = await manager_ctrl.profile(req.params.mid);
    responseFormat(res, 200, "success", creatUserResult);
  } catch (error) {
    console.log(error);
    responseFormat(res, 409, "failed", {
      error: error,
    });
  }
});

router.put(
  "/:mid/changepassword",
  [
    check("oldpassword", "Old Password is required")
      .isLength({ min: 1 })
      .exists(),
    check("newpassword", "New Password is required")
      .isLength({ min: 1 })
      .exists(),
  ],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      let responseData = await manager_ctrl.changepassword(
        req.params.mid,
        req.body.oldpassword,
        req.body.newpassword
      );
      if (responseData) {
        responseFormat(res, 200, "success", responseData);
      } else {
        console.log(error);
        responseFormat(res, 409, "failed", responseData);
      }
    } catch (error) {
      console.log(error);
      if (error.constraint == null && error.message == undefined) {
        responseFormat(res, 409, "failed", {
          error: error,
        });
      } else if (error.message != undefined) {
        responseFormat(res, 409, "failed", {
          error: error.mapped(),
        });
      } else {
        let errName = error.constraint.split("_");
        responseFormat(res, 422, "failed", {
          error: `${errName[2]} already exists`,
        });
      }
    }
  }
);

module.exports = router;
