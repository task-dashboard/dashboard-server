var express = require("express");
var router = express.Router();
const { check, oneOf, validationResult } = require("express-validator/check");
const worker_ctrl = require("../controllers/worker");
const task_ctrl = require("../controllers/task");

router.post(
  "/login",
  [
    check("email", "email is required").isEmail().isLength({ min: 1 }).exists(),
    check("password", "password is required").isLength({ min: 1 }).exists(),
  ],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      const email = req.body.email;
      const password = req.body.password;
      let responseData = await worker_ctrl.login(email, password);
      if (responseData) {
        responseFormat(res, 200, "success", responseData);
      } else {
        console.log(error);
        responseFormat(res, 409, "failed", responseData);
      }
    } catch (error) {
      console.log(error);
      responseFormat(res, 409, "failed", error);
    }
  }
);

router.get(
    "/:wid/task",
    async (req, res, next) => {
      try {
        taskstatus = false;
        if(req.query.status){
            taskstatus = req.query.status
        }
        let creatUserResult = await task_ctrl.getWorkerTasks(req.params.wid, taskstatus, req.query);
        responseFormat(res, 200, "success", creatUserResult);
      } catch (error) {
        console.log(error);
        if (error.constraint == null && error.message == undefined) {
          responseFormat(res, 409, "failed", {
            error: error,
          });
        } else if (error.message != undefined) {
          responseFormat(res, 409, "failed", {
            error: error.mapped(),
          });
        } else {
          let errName = error.constraint.split("_");
          responseFormat(res, 422, "failed", {
            error: `${errName[2]} already exists`,
          });
        }
      }
    }
);


router.get(
    "/:wid/profile",
    async (req, res, next) => {
      try {
        let creatUserResult = await worker_ctrl.profile(req.params.wid);
        responseFormat(res, 200, "success", creatUserResult);
      } catch (error) {
        console.log(error);
        responseFormat(res, 409, "failed", {
            error: error,
        });
      }
    }
);


router.put(
    "/:wid/changepassword",
    [
      check("oldpassword", "Old Password is required").isLength({ min: 1 }).exists(),
      check("newpassword", "New Password is required").isLength({ min: 1 }).exists(),
    ],
    async (req, res, next) => {
      try {
        validationResult(req).throw();
        let responseData = await worker_ctrl.changepassword(req.params.wid, req.body.oldpassword, req.body.newpassword);
        if (responseData) {
          responseFormat(res, 200, "success", responseData);
        } else {
          console.log(error);
          responseFormat(res, 409, "failed", responseData);
        }
      } catch (error) {
        console.log(error);
        if (error.constraint == null && error.message == undefined) {
          responseFormat(res, 409, "failed", {
            error: error,
          });
        } else if (error.message != undefined) {
          responseFormat(res, 409, "failed", {
            error: error.mapped(),
          });
        } else {
          let errName = error.constraint.split("_");
          responseFormat(res, 422, "failed", {
            error: `${errName[2]} already exists`,
          });
        }
      }
    }
  );
  

module.exports = router;
