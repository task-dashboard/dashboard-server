require("dotenv").config(); //instatiate environment variables

CONFIG = {}; //Make this global to use all over the application
CONFIG.token_secret= process.env.TOKEN_SECRET;
CONFIG.app = process.env.APP || "dev";
CONFIG.port = process.env.PORT || "3000";

/**
 * @param {*} res
 * @param {number} statuscode
 * @param {string} msg
 * @param {*} result
 */
responseFormat = async (res, statuscode, msg, result) => {
    res.statusCode = 400;
    // Success Web Response
    if (typeof statuscode !== "undefined") res.statusCode = statuscode;
    return res.json({
      responseCode: statuscode,
      responseMessage: msg,
      response: result
    });
  };