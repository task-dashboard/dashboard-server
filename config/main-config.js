(function (appConfig) {
  "use strict";

  // *** main dependencies *** //
  const createError = require("http-errors");
  const express = require("express");
  const path = require("path");
  const cookieParser = require("cookie-parser");
  // const logger = require("morgan");
  const bodyParser = require("body-parser");
  var flash = require("connect-flash");
  var session = require("express-session");
  var moment = require("moment");
  var _ = require("lodash");
  // var passport = require("passport");
  // var JwtStrategy = require("passport-jwt").Strategy;
  // var ExtractJwt = require("passport-jwt").ExtractJwt;
  var cors = require('cors')

  // *** load environment variables *** //
  require("./config");
  // require("../helper/global_functions");

  appConfig.init = function (app, express) {
    // view engine setup
    app.set("views", path.join(__dirname, "../views"));
    app.set("view engine", "ejs");

    app.use(cors())
    // app.use(logger("dev"));
    app.use(express.json());
    app.use(
      express.urlencoded({
        extended: true
      })
    );
    app.use(
      bodyParser.urlencoded({
        extended: true
      })
    );
    app.use(cookieParser("secret"));
    app.use(
      session({
        secret: "task_manager",
        proxy: true,
        resave: true,
        saveUninitialized: true
      })
    );
    app.use(flash());
    /**bodyParser.json(options)
     * Parses the text as JSON and exposes the resulting object on req.body.
     */
    app.use(function (req, res, next) {
      res.locals.flashdata = req.flash();
      res.locals.moment = moment;
      res.locals._ = _;
      res.locals.baseURL = req.headers.host;
      next();
    });
    app.use(bodyParser.json());
    app.use(express.static(path.join(__dirname, "../public")));

    // app.use(passport.initialize());
    // app.use(passport.session());
    // *** cross domain requests *** //
    const allowCrossDomain = function (req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header(
        "Access-Control-Allow-Methods",
        "GET,POST,DELETE,OPTIONS,PUT,PATCH"
      );
      res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
      res.header("Access-Control-Allow-Headers", "content-type, authorization");
      res.header("Access-Control-Expose-Headers", "x-filename");
      next();
    };
    app.use(allowCrossDomain);
    // passport.serializeUser(function (user, done) {
    //   done(null, user);
    // });

    // passport.deserializeUser(function (user, done) {
    //   done(null, user);
    // });

    // var opts = {};
    // // Setup JWT options
    // opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
    // opts.secretOrKey = CONFIG.token_secret;

    // passport.use(
    //   new JwtStrategy(opts, function (jwtPayload, done) {
    //     //If the token has expiration, raise unauthorized
    //     // var expirationDate = new Date(jwtPayload.exp * 1000)
    //     // if(expirationDate < new Date()) {
    //     //   console.log(expirationDate)
    //     //   return done(null, false);
    //     // }
    //     var user = jwtPayload;
    //     // console.log(user)
    //     // done(null, user)
    //     const now = moment().unix();
    //     // check if the token has expired
    //     if (now > jwtPayload.exp) done("Token has expired.");
    //     else done(null, user);
    //   })
    // );
  };
})(module.exports);