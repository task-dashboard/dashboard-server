(function(routeConfig) {
  "use strict";

  routeConfig.init = function(app) {
    // *** routes *** //
    var managerRoutes = require("../routes/manager");
    var workerRoutes = require("../routes/worker");
    
    app.use("/manager", managerRoutes);
    app.use("/worker", workerRoutes);
  };
})(module.exports);
